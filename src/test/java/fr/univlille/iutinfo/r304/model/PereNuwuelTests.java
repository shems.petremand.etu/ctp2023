package fr.univlille.iutinfo.r304.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PereNuwuelTests {
    PereNuwuel pereNuwuel = new PereNuwuel();
    Participant p1 = new Participant("Oui");
    Participant p2 = new Participant("Non");
    Participant p3 = new Participant("AAH");

    @BeforeEach void init(){
        pereNuwuel = new PereNuwuel();
    }

    @Test
    public void test_ajouterParticipant() {
        assertTrue(pereNuwuel.ajouterParticipant(p1));
        assertEquals(1, pereNuwuel.size());

        assertFalse(pereNuwuel.ajouterParticipant(p1));
        assertEquals(1, pereNuwuel.size());
    }

    @Test
    public void test_toString(){
        assertEquals("", pereNuwuel.toString());
        pereNuwuel.ajouterParticipant(p1);
        assertEquals("Oui\n", pereNuwuel.toString());
        pereNuwuel.ajouterParticipant(p2);
        assertEquals("Oui\nNon\n", pereNuwuel.toString());
    }

    @Test void test_affichageCirculaire(){
        basicSetup();
        assertEquals("Oui\n->Non\n->AAH", pereNuwuel.affichageCirculaire());
    }

    @Test void test_afficahgeAlphabetique(){
        basicSetup();
        assertEquals("AAH -> Oui\nNon -> AAH\nOui -> Non\n", pereNuwuel.affichageAlphabetique());
    }

    @Test void test_assignerParticipants(){
        initParticipants();

        pereNuwuelSetup();

        pereNuwuel.assignerParticipants();

        assertFalse(p1.getReceveur() == null);
        assertFalse(p2.getReceveur() == null);
        assertFalse(p3.getReceveur() == null);
    }

    private void initParticipants() {
        p1.reinitialiserReceveur();
        p2.reinitialiserReceveur();
        p3.reinitialiserReceveur();
    }

    private void basicSetup() {
        p1.assignerReceveur(p2);
        p2.assignerReceveur(p3);
        p3.assignerReceveur(p1);

        pereNuwuelSetup();
    }

    private void pereNuwuelSetup() {
        pereNuwuel.ajouterParticipant(p1);
        pereNuwuel.ajouterParticipant(p2);
        pereNuwuel.ajouterParticipant(p3);
    }


}
