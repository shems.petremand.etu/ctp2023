package fr.univlille.iutinfo.r304.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParticipantTests {
    Participant p1 = new Participant("Oui");
    Participant p2 = new Participant("Non");
    Participant p3 = new Participant("AAH");

    @Test public void test_assignerReceveur(){
        assertTrue(p1.assignerReceveur(p2));
        assertTrue(p2.assignerReceveur(p1));
        assertFalse(p1.assignerReceveur(p2));
        assertFalse(p1.assignerReceveur(p3));
    }

    @Test public void test_reinitialiserReceveur() {
        p1.assignerReceveur(p2);
        p1.reinitialiserReceveur();

        assertEquals(null, p1.getReceveur());
    }
}
