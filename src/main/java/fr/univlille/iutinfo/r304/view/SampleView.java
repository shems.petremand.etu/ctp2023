package fr.univlille.iutinfo.r304.view;

import fr.univlille.iutinfo.r304.model.Participant;
import fr.univlille.iutinfo.r304.model.PereNuwuel;
import fr.univlille.iutinfo.r304.model.utils.Observer;
import fr.univlille.iutinfo.r304.model.utils.Subject;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SampleView extends Application implements Observer {
    Label participants = new Label();
    PereNuwuel pereNuwuel = new PereNuwuel();
    Label circularTitle = new Label("Affichage circulaire");
    Label alphaTitle = new Label("Affichage alphabétique");
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        pereNuwuel.attach(this);

        VBox left = new VBox();
        Button addBtn = new Button();
        Label title = new Label("Inscrits :");
        TextField input = new TextField();
        input.setMinSize(10, 30);
        addBtn.setText("+");
        addBtn.setOnAction(event -> pereNuwuel.ajouterParticipant(new Participant(input.getText())));
        left.getChildren().addAll(title, input, participants, addBtn);

        VBox right = new VBox();
        Button btn = new Button();
        btn.setText("Nouveau tirage !");

        btn.setOnAction(event -> pereNuwuel.assignerParticipants());


        TextArea circularContent = new TextArea();
        circularContent.appendText("Example1");
        circularContent.appendText("\n -> Example2");
        circularContent.setPrefSize(250,100);

        TextArea alphaContent = new TextArea();
        alphaContent.setPrefSize(250,100);
        alphaContent.appendText("Example1 -> Example2");
        right.getChildren().addAll(btn, circularTitle, circularContent, alphaTitle, alphaContent);

        HBox root = new HBox(left, right);
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setTitle("Tirage au sort des cadeaux");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void update(Subject subj) {
        circularTitle.setText(pereNuwuel.affichageCirculaire());
        alphaTitle.setText(pereNuwuel.affichageAlphabetique());
    }

    @Override
    public void update(Subject subj, Object data) {
        if(data instanceof String){
            String s = (String) data;
            participants.setText(s);
        }
    }
}

