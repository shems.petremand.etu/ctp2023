package fr.univlille.iutinfo.r304.model.utils;

public interface Observer {
    public void update(Subject subj);
    public void update(Subject subj, Object data);
}

