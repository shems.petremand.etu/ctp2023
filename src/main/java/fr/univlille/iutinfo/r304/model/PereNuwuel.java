package fr.univlille.iutinfo.r304.model;

import fr.univlille.iutinfo.r304.model.utils.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PereNuwuel extends Subject {
    private List<Participant> participants;

    public PereNuwuel() {
        this.participants = new ArrayList<>();
    }

    public List<Participant> getParticipant() {
        return this.participants;
    }

    public int size(){
        return this.participants.size();
    }

    public boolean ajouterParticipant(Participant p){
        if(this.participants.contains(p))
            return false;

        this.participants.add(p);
        notifyObservers(toString());
        return true;
    }

    public String toString(){
        StringBuilder s = new StringBuilder();
        for(Participant p : participants){
            s.append(p.getNom()).append("\n");
        }

        return s.toString();
    }

    public String affichageCirculaire(){
        Participant init = participants.get(0);

        StringBuilder s = new StringBuilder(init.getNom());

        Participant courrant = init.getReceveur();
        s.append("\n->").append(courrant.getNom());

        while(courrant.getReceveur() != init){
            courrant = courrant.getReceveur();
            s.append("\n->").append(courrant.getNom());
        }

        return s.toString();
    }

    public String affichageAlphabetique(){
        StringBuilder s = new StringBuilder();

        ArrayList<Participant> triee = new ArrayList<>(this.participants);
        Collections.sort(triee);

        for(Participant p : triee){
            s.append(p.getNom()).append(" -> ").append(p.getReceveur().getNom()).append("\n");
        }

        return s.toString();
    }

    public void assignerParticipants(){
        List<Participant> nonAssignes = new ArrayList<>(this.participants);

        for(Participant p : this.participants){
            Participant receveur = nonAssignes.get(new Random().nextInt(nonAssignes.size()));
            nonAssignes.remove(receveur);
            p.assignerReceveur(receveur);
        }

        notifyObservers();
    }
}
