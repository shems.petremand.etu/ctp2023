package fr.univlille.iutinfo.r304.model;

public class Participant implements Comparable<Participant> {
    private String nom;
    private Participant receveur;

    public Participant(String nom, Participant receveur) {
        this.nom = nom;
        this.receveur = receveur;
    }

    public Participant(String nom){
        this.nom = nom;
        this.receveur = null;
    }

    public String getNom() {
        return nom;
    }

    public Participant getReceveur() {
        return receveur;
    }

    public boolean assignerReceveur(Participant receveur) {
        if(this.receveur != null)
            return false;

        this.receveur = receveur;
        return true;
    }

    public void reinitialiserReceveur() {
        this.receveur = null;
    }


    @Override
    public int compareTo(Participant p) {
        return this.nom.compareTo(p.nom);
    }
}
